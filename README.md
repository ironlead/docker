# README #

Aqui encontraras los conocimientos adquiridos en tu curso de docker.

### DOCKER ###

* Docker es una herramienta que permite desplegar aplicaciones de una forma rapida y portable

### Arquitectura Docker ###

Docker se compone de:

* Docker Host: Docker host puede ser nuestro propo computador (en caso de tener un laboratorio local), tambien puede ser el servidor donde se instale docker.  
El docker host a su vez contiene:
 -  Docker CLI => a traves de este podemos ejecutar instrucciones que utilizan al REST API. Con el CLI podemos manejar CONTENEDORES, IMAGENES, VOLUMENES, REDES. 
 -  REST API => es utilizado por Docker CLI y este a su vez canaliza las instrucciones al Docker Daemon Server. Es Basicamente el canal de comunicacion entre Docker CLI y el Server.
 -  Docker Daemon Server => es quien presta el servicio de docker propiamente. Es un servicio que esta corriendo.